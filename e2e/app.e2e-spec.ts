import { GitExamplePage } from './app.po';

describe('git-example App', () => {
  let page: GitExamplePage;

  beforeEach(() => {
    page = new GitExamplePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
