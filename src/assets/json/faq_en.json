[
  {
    "title": "What is CastifyMe?",
    "content": "CastifyMe is a job portal for extras, actors, and models. Casting directors can post jobs and ask actors for a written application with a link to the actor’s profile – or send the actor a manuscript and ask the actor to cast himself. The actor sends the tape back directly to the casting director, who has a custom-designed media player with built-in smart functions.",
    "site": true
  },
  {
    "title": "Why choose CastifyMe?",
    "content": "There has been practically no development on casting sites over the past 20 years, neither as far as pricing, nor as far as options.<br><br>The standards for a modern casting site should be significantly higher, and like everything else online, the prices should be reduced for the benefit of the actors and the film industry. To maximize your job opportunities, you should combine several casting sites at the same time….<br><br>Therefore, a subscription to CastifyMe costs far less than half of what other casting sites charge.<br><br> At the same time, CastifyMe has something that no other casting sites have: The option of casting yourself.<br><br> We have also taken the time to research the entire casting process used by casting directors, and we are therefore the only casting site to develop and expand the search criteria for finding the right actors, such as by type or experience.<br><br> This makes it much easier for casting directors to find the right actors, and they no longer have to search for you like looking for a needle in a haystack.",
    "site": true
  },
  {
    "title": "Who is CastifyMe targeting?",
    "content": "The website is for children and adults who interested in acting in movies and on TV. If you have a subscription to another casting site, our low prices will enable you to combine with other casting sites.<br><br> CastifyMe is particularly ideal for unknown talent with another job outside of the entertainment industry. Moreover, CastifyMe is obviously intended for employers in the film and TV industry.",
    "site": true
  },
  {
    "title": "Where do I see the job postings?",
    "content": "You find all job postings on your job page when you log in.",
    "site": true
  },
  {
    "title": "Can I apply for jobs without having to create my own casting?",
    "content": "Yes, but it depends what the producer wants. If the producer isn’t looking for a tape, you can just apply in writing and with your profile.",
    "site": true
  },
  {
    "title": "Which country should I say I’m from if I have dual nationalities?",
    "content": "Write down the country where you live and want to work.",
    "site": true
  },
  {
    "title": "Why provide information about any other occupation and my type?",
    "content": "When casting directors look for unknown actors, they do so to give a project authenticity. Therefore, they often look for actors, who are as close as possible to the role they are casting. They are often looking for people who have specific abilities reflected in the role. This could be in sports, profession, or music. And they are frequently looking for a type, for instance a “hipster”. Therefore, CastifyMe also lets you note which type you resemble at first glance.",
    "site": true
  },
  {
    "title": "Which type am I?",
    "content": "Imagine what kind of role you might play or ask your friends and family. Don’t take it too literally….<br><br> If you are under 16, you don’t need to write your type. Kids are kids!",
    "site": true
  },
  {
    "title": "How good do I need to be at a sport or playing an instrument?",
    "content": "You need to be a little bit better than a beginner – meaning you’ve got the techniques down.",
    "site": true
  },
  {
    "title": "Can CastifyMe give me an answer to why I didn’t get a role?",
    "content": "No. CastifyMe merely checks that the job posting is consistent with the rules before it is posted. The site has no influence as to which applicants are rejected or why.",
    "site": true
  },
  {
    "title": "Is CastifyMe an employer or an agency?",
    "content": "Neither, CastifyMe simply connects employers with actors.",
    "site": true
  },
  {
    "title": "Can I write to the casting directors?",
    "content": "You can only write to the casting directors, if you have applied for a project.",
    "site": true
  },
  {
    "title": "Where do I find messages from the casting directors?",
    "content": "Messages at CastifyMe are always tied to your applications. You click on a project you have applied for and find your messages in your application. If you delete your application, you will no longer be able to see your messages.",
    "site": true
  },
  {
    "title": "What is a self-tape?",
    "content": "A self-tape is a video of an actor’s casting, which the actor makes himself to send to the casting director.",
    "site": true
  },
  {
    "title": "Can I use things besides a smartphone to cast myself?",
    "content": "Sure, you can. Cameras, tablets, whatever you want. The phone is just easier to use, and the quality is perfect.",
    "site": true
  },
  {
    "title": "Why is there no CastifyMe app?",
    "content": "CastifyMe chose to make the website responsive, rather than creating an app. Responsive means that the website looks perfect in your phone’s browser. That’s not to say that there’ll never be an app…",
    "site": true
  },
  {
    "title": "What is a self-tape/self-cast?",
    "content": "Casting directors often ask actors to make a self-tape. They send them the manuscript, and the actor films himself playing the role. A smartphone is usually perfect for this. Then, the actor sends his video/self-tape to the casting director.",
    "site": true
  },
  {
    "title": "How do I make a self-tape?",
    "content": "First, learn your lines, so you are ready to have a great casting!<br><br>Film yourself against a white background and not in backlight.<br><br>Place the camera in eye-height on something like a shelf or a tripod. (DON’T HOLD THE CAMERA IN YOUR HAND!).<br><br> Film yourself so you are visible from your navel up.<br><br> Keep your phone horizontal!<br><br> Always make a test before recording your casting.",
    "site": true
  },
  {
    "title": "Why do I need to hold the phone horizontally when recording?",
    "content": "Because it makes for better quality. Keeping it vertical makes it unbearable to look at….",
    "site": true
  },
  {
    "title": "Which security measures does CastifyMe have in place?",
    "content": "CastifyMe always verifies the producers, checking all their submitted information. Only afterwards can the producers use the website.<br><br>When an employer posts a job, CastifyMe always verifies the job posting before it reaches the actors. Similarly, CastifyMe checks all communication to actors under 18.<br><br>Even so, CastifyMe expect users to use their common sense as well, and that they notify us if they experience anything uncomfortable. We want to emphasize that users under age 18 need the permission of their parents or guardian, and we recommend that all contact information be that of a parent or guardian.<br><br>If CastifyMe finds that employers are skirting our rules, they will be excluded from CastifyMe without notice. If relevant, CastifyMe will contact the police, guardian, or parents and provide any printouts of messages and postings, should the police, guardian, or parents want them.",
    "site": true
  },
  {
    "title": "Is there a minimum contract period for a CastifyMe subscription?",
    "content": "You are charged for three months at a time at the beginning of that period. You can cancel your subscription any time, however you cannot get back the money for the three months, you have already prepaid.",
    "site": true
  },
  {
    "title": "What happens to my job applications if I cancel my subscription before receiving a reply from the producer?",
    "content": "The producer will not be able to see your application, since your profile no longer exists. Instead, we recommend that your downgrade your membership to Star if you’re currently at Superstar.",
    "site": true
  },
  {
    "title": "What happens to my job applications if I downgrade my subscription to Hangaround before receiving a reply from the producer?",
    "content": "It is no problem if you downgrade from a Superstar membership to a Star membership as far as it impacting your applications. However, if you downgrade to Hangaround, your applications will no longer be visible to the producer.",
    "site": true
  },
  {
    "title": "How do I change my email?",
    "content": "Go to Edit Profile, where you’ll find “Email Management”. Here, you can add a new email address.<br><br>Next, you need to confirm your newly added email address, before you can start using it on CastifyMe as your “Primary E-mail”. This is done via the link sent to the newly added email address.<br><br>Once you have confirmed your new email address, you can easily make it your “Primary Email” in Edit Profile / Email Management.",
    "site": true
  },
  {
    "title": "Why does my subscription not change on my profile when I downgrade?",
    "content": "Remember that you’ve paid for three months. These three months need to pass before your membership changes.",
    "site": true
  },
  {
    "title": "What happens to my self tape and who sees it?",
    "content": "It lands on the casting director’s personal dashboard on Selftape Scandinavia. It is deleted one 30 days after call back and can´t be restored.<br><br>The user who created the casting call can see it and he can share an encrypted link with a client.  The filmmaker agrees to handle all self-tapes he receives in his inbox with discretion, and he agrees only to watch the self-tapes via CastifyMe.<br><br>The filmmaker agrees not to rip, download, or share the self-tapes on the website under any circumstances or in any media, on the Internet, or in any future medias.<br><br>Should the website detect any instances of fraud, system abuse, defamation, or any type of activity deemed to be inappropriate or illegal in the sole discretion of the website, these actions may result in member termination and possible legal action.",
    "site": true
  },
  {
    "title": "Can self taping and CastifyMe replace casting directors?",
    "content": "No. This is for pre-casting mostly. You will never get the same impression of an actor from a self-tape. It can, however, give the casting director an impression of who is out there. A casting director can help you with creative feedback and help the actors to make the right choices when they perform. Likewise, they can keep your mind open to other options that you never thought of. We recommend that you or your casting director use CastifyMe to do a pre-casting and then follow up with real, live castings.",
    "site": true
  },
  {
    "title": "I´ve just signed up but I can´t sign in?",
    "content": "You probably forgot to verify your email. When you sign up, you get an email that verifies your account. You’ll have to respond to it before you are able to sign in.<br><br>Check your spam folder as well if you can’t find the email in your inbox.",
    "site": true
  },
  {
    "title": "I still cant sign in?!",
    "content": "Did you verify your email? Otherwise, try one of the following solutions:<br><br><ul><li>Enable cookies. (Usually, that solves the problem.)</li><li>Clear your browser’s history.</li><li>Check extensions and plugins.</li><li>Remove corrupt cookies file.</li></ul>",
    "site": true
  },
  {
    "title": "My verification email isn´t in my mailbox?",
    "content": "Check your spam folder.",
    "site": true
  },
  {
    "title": "Can I use a camcorder to shoot my self tapes?",
    "content": "Well, yes...but why?!  Its much easier just to use your smartphone, webcam or tablet. The quality is perfect, so don ́t worry",
    "site": true
  },
  {
    "title": "Whats up with all this typecasting and working experience?",
    "content": "Most casting sites base their search functions on the weight, height, hair color, and eye color of the actors. That’s fine if you are casting models (!) - but 80% of casting for movie roles is about TYPES and LIFE EXPERIENCE.<br><br>The search functions at CastifyMe are like no other website’s in that they’re based on the way casters actually look for actors. Lots of actors have another job in addition to their craft, and that can be a huge advantage. After all, actors portray reality, and their experiences can provide a role with the needed believability. With the technology of today, you can even cast yourself online with your smartphone. That solves the problem of unknown actors never being discovered or going on vacation and suddenly being offered a casting.",
    "site": true
  }

]
