import { Component, OnInit } from '@angular/core';
import {ModalComunicationService} from "../modal-comunication.service";

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor(private _modalComunication: ModalComunicationService) {

  }

  announce() {
    this._modalComunication.announceMission('modal');

  }

  ngOnInit() {
  }

}
