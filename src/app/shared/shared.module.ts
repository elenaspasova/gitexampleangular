import {NgModule} from "@angular/core";
// import {TruncatePipe} from "../pipes/truncate.pipe";
import {RegisterModalComponent} from "../reusable/register-modal/register-modal.component";
import {FooterComponent} from "../reusable/footer/footer.component";
import {MenuComponent} from "../reusable/menu/menu.component";
import {RouterModule} from "@angular/router";
// import {TranslateDirective} from "../translate.directive";
import {AlertModule, ModalModule, ButtonsModule, BsDropdownModule, DatepickerModule} from "ng2-bootstrap";
import {FormsModule} from "@angular/forms";
import {CommonModule, DatePipe} from "@angular/common";
// import {ImagePreview} from "../jobs/image-preview.directive";
// import {FileDropDirective, FileSelectDirective} from "ng2-file-upload";
import {ModalComunicationService} from "../modal-comunication.service";
import {AuthGuard} from "../auth-guard.service";
// import {TranslateService} from "../translate.service";
import {appConfig} from "../config/app.config";
import {ComService} from "../com-service.service";
import {MultiselectDropdownModule} from "angular-2-dropdown-multiselect/src/multiselect-dropdown";
// import {SafePipe} from "../safe.pipe";
// import {SafeHtmlPipe} from "../safe-html.pipe";
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    AlertModule,
    FormsModule,
    ModalModule,
    MultiselectDropdownModule,
    ButtonsModule,
    BsDropdownModule,
    DatepickerModule
  ],
  providers: [ModalComunicationService, ComService, appConfig, AuthGuard, DatePipe],
  declarations: [MenuComponent, FooterComponent, RegisterModalComponent, ],
  exports: [MenuComponent, FooterComponent, RegisterModalComponent]
})
export class SharedModule {
}
