import {Component, OnInit, ApplicationRef} from "@angular/core";
import {appConfig} from "./config/app.config";
import {ComService} from "./com-service.service";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {ModalComunicationService} from "./modal-comunication.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app works!';
  activeLang = localStorage.getItem('activeLang');
  lang = null;
  subscription;


  constructor(private _modalComunication: ModalComunicationService,
              private activatedRoute: ActivatedRoute,
              private _config: appConfig,
              private _comService: ComService,
              private _ApplicationRef: ApplicationRef,
              private router: Router,
  ) {
    localStorage.removeItem('talent_loaded');// filter on talent page
    // this.findMission();
    if (this.activeLang === null) {
      this.activeLang = 'en';
      localStorage.setItem('activeLang', this.activeLang);
      this.updateLang();

    }
    _comService.isLoggedIn(true);
    this.subscription = this.activatedRoute.queryParams.subscribe(
      (queryParams: any) => {
        if (queryParams['lang'] !== undefined) {

          this.activeLang = queryParams['lang'];
          localStorage.setItem('activeLang', this.activeLang);
          this.updateLang();

        }

      });
  }

  // findMission() {
  //     this._modalComunication.announceMission('pi6ka');
  //     console.log('this._modalComunication.announceMission',this._modalComunication.announceMission);
  //     return;
  //   }

  updateLang() {
    if (this.activeLang === 'en') {
      this._modalComunication.announceMission('lang');
      return;
    }
    if (localStorage.getItem('lang_' + this.activeLang) !== null) {
      this._comService.getTR('/assets/lang/' + this.activeLang + '.json', this._config.baseURI)
        .subscribe(
          res => {

            let hdr = res.headers;
            let lastModified = hdr.get('last-modified');
            if (lastModified) {
              let lastModifiedDate = +new Date(lastModified);
              let storageDate = parseInt(localStorage.getItem('date_modified_'+this.activeLang), 10);

              if (isNaN(storageDate) || storageDate < lastModifiedDate) {
                localStorage.removeItem('lang_da');
                localStorage.removeItem('lang_sv');
                localStorage.removeItem('lang_no');
                window.location.reload(true);
                return;
              } else {
                this._modalComunication.announceMission('lang');
                return;
              }

            }

          }
        );
    } else {
      this._comService.get('/assets/lang/' + this.activeLang + '.json', this._config.baseURI).subscribe(
        res => {
          localStorage.setItem('lang_' + this.activeLang, JSON.stringify(res));
          localStorage.setItem('date_modified_'+this.activeLang,String(+new Date()));
          this._modalComunication.announceMission('lang');

        }
      );
    }
  }

  ngOnInit() {
    if (this.activeLang === 'en') return;


    if (localStorage.getItem('lang_' + this.activeLang) !== null) {
      this._modalComunication.announceMission('lang');
      return;
    }

    this._comService.get('/assets/lang/' + this.activeLang + '.json', this._config.baseURI).subscribe(
      res => {
        localStorage.setItem('lang_' + this.activeLang, JSON.stringify(res));
        this._modalComunication.announceMission('lang');
      }
    );

  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
