import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {appRoutes} from './app.routing';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {AboutComponent} from './about/about.component';
import {MultiselectDropdownModule} from "angular-2-dropdown-multiselect/src/multiselect-dropdown";
import {SharedModule} from './shared/shared.module';
import {AlertModule, BsDropdownModule, ButtonsModule, ModalModule} from 'ng2-bootstrap';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    MultiselectDropdownModule,
    SharedModule,
    RouterModule.forRoot(appRoutes),
    ModalModule.forRoot(),
    // ShareButtonsModule.forRoot(),
    // DatepickerModule.forRoot(),
    // PopoverModule.forRoot(),
    BsDropdownModule.forRoot(),
    ButtonsModule.forRoot(),
    // TabsModule.forRoot(),
    // AccordionModule.forRoot(),forRoot

    AlertModule.forRoot(),


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
