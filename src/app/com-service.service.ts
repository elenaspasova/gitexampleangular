import {Injectable} from "@angular/core";
// import "./rxjs-operators";
 import {appConfig} from "./config/app.config";
import {Headers, Http, Response} from "@angular/http";
import {Router} from "@angular/router";
import {ModalComunicationService} from "./modal-comunication.service";
import {Observable} from "rxjs";
import 'rxjs/add/observable/of';
//import {TranslateService} from "./translate.service";
@Injectable()
export class ComService {
  API = this._conf.API;

  constructor(
    private _conf: appConfig,
    public _http: Http,
    private router: Router,
    private _modalComunication: ModalComunicationService,
   // private _TranslateService: TranslateService
  ) {
  }
  activeLang = localStorage.getItem('activeLang');
  //
  // translateInlineMessages(textToTranslate){
  //   if (this.activeLang === 'en') return textToTranslate;
  //
  //   let lang = JSON.parse(localStorage.getItem('lang_' + this.activeLang));
  //   if (!lang[textToTranslate]) {
  //     lang[textToTranslate] = 'Missing Translation';
  //   }
  //   return lang[textToTranslate];
  // }

  isEmpty(obj) {
  for(var key in obj) {
    if(obj.hasOwnProperty(key))
      return false;
  }
  return true;
}

  get(URI, altAPI?: any, Object?: any, hdrs?: any) {
    let API = this.API;
    if (altAPI) {
      API = altAPI;
    }

    if (!this.isEmpty(Object)) {
      let additionalURI = '';
      Object.forEach((element, index, array) => {
        for (var key in element) {

          if (element.hasOwnProperty(key)) {

            additionalURI += '&' + key + '=' + encodeURIComponent(element[key]);

          }
        }
      });
      URI = URI + additionalURI;
    }
    let headers = new Headers();
    if (hdrs !== true) {
      headers.append('Authorization', localStorage.getItem('token'));

    } else {
      headers.append('X-Requested-With', 'text/html;');
    }


    return this._http.get(API + URI, {headers: headers})
    // ...and calling .json() on the response to return data
      .map((res: Response) => res.json());
    //...errors if any


  }

  getTR(URI, altAPI?: any, Object?: any, hdrs?: any) {
    let API = this.API;
    if (altAPI) {
      API = altAPI;
    }

    if (!this.isEmpty(Object)) {
      let additionalURI = '';
      Object.forEach((element, index, array) => {
        for (var key in element) {

          if (element.hasOwnProperty(key)) {

            additionalURI += '&' + key + '=' + encodeURIComponent(element[key]);

          }
        }
      });
      URI = URI + additionalURI;
    }
    let headers = new Headers();
    if (hdrs !== true) {
      headers.append('Authorization', localStorage.getItem('token'));

    } else {
      headers.append('X-Requested-With', 'text/html;');
    }


    return this._http.get(API + URI, {headers: headers})
    // ...and calling .json() on the response to return data
      .map((res: Response) => res);
    //...errors if any


  }

  // setPreset(video_id){
  //
  //   let headers = new Headers();
  //   let API = `https://api.vimeo.com/videos/${video_id}/privacy/domains/castifyme.com`;
  //   let APIlocalhost = `https://api.vimeo.com/videos/${video_id}/privacy/domains/localhost`;
  //   let APIApplyPreset = `https://api.vimeo.com/videos/${video_id}/presets/120421283`;
  //   headers.append('Authorization', 'Bearer ' + this._conf.vimeoToken);
  //
  //   this._http.put(API,
  //     {}, {headers: headers})
  //     .subscribe();
  //   this._http.put(APIlocalhost,
  //     {}, {headers: headers})
  //     .subscribe();
  //   this._http.put(APIApplyPreset,
  //     {}, {headers: headers})
  //     .subscribe();
  // }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

  isLoggedIn(isAppComponent?) {
    let token = localStorage.getItem('token');
    if (!(!!token)){
      if (isAppComponent) return;
      window.location.href = '/';
      return;
    }

    this.get('/protected').subscribe(
      res => {
        if ((!res.success) && (res.message === 'Missing token.' || res.message === 'Token expired.')){
          localStorage.removeItem('token');
          localStorage.removeItem('me');
          window.location.href = '/';
        } else {
          if (res.message.description) {
            res.message.description = res.message.description.replace(/<br\s*\/?>/mg,"\n");
          }
          localStorage.setItem('me',JSON.stringify(res.message));
          let me = JSON.parse(localStorage.getItem('me'));
          this._modalComunication.announceMission('login');

         if (me && me.role === 0 && me.trial_offered === true && me.activeSubscription ==='_free'  && this.router.url != '/jobs' && this.router.url != '/profile/' + me._id) {
           console.log('this.router.url',this.router.url);
             this.router.navigate(['/payments']);
          }

         else if (me && me.role === 0 && me.trial_offered === true && me.activeSubscription ==='_free' && this.router.url == '/jobs') {
           this.get('/get_notifications_actor').subscribe(
             res => {
               if (res.success) {
                 localStorage.setItem('notifications',JSON.stringify(res.message));
                 this._modalComunication.announceMission('notifications');
               }
             });
           this.router.navigate(['/jobs']);
         }

         else if (me && me.role === 0 && me.trial_offered === true && me.activeSubscription ==='_free' && this.router.url == '/profile/' + me._id) {
           this.get('/get_notifications_actor').subscribe(
             res => {
               if (res.success) {
                 localStorage.setItem('notifications',JSON.stringify(res.message));
                 this._modalComunication.announceMission('notifications');
               }
             });
           this.router.navigate(['/profile/' + me._id]);
         }

         else if (me && me.role === 0  && this.router.url != '/profile/' + me._id) {
            this.get('/get_notifications_actor').subscribe(
              res => {
                if (res.success) {
                  localStorage.setItem('notifications',JSON.stringify(res.message));
                  this._modalComunication.announceMission('notifications');
                }
              });
            this.router.navigate(['/jobs']);
          }



          else if (me && me.role === 1 && (window.location.href.toString().replace('profile','') == window.location.href.toString())){
            this.router.navigate(['/dashboard']);
            this.get('/get_notifications').subscribe(
              res => {
                if (res.success) {
                  localStorage.setItem('notifications',JSON.stringify(res.message));
                  this._modalComunication.announceMission('notifications');
                }
              });
          }


        }
      }
    );


  }


  post(URI, Object, skipAuth?: boolean) {
    let headers = new Headers();

    if (!skipAuth) {
      headers.append('Authorization', localStorage.getItem('token'));
    }

    return this._http.post(this.API + URI,
      {
        Object
      }, {headers: headers})
    // ...and calling .json() on the response to return data
      .map((res: Response) => res.json());
    //...errors if any


  }

  confirmUserByEmail(Object) {
    return this._http.post(this.API + '/confirm_user_by_email',
      {
        hash: Object.hash,
        userId: Object.userId
      }
    )
      .map(res => res.json());
    //this.translateInlineMessages(res.message);
  }

}
