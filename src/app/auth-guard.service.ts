import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate() {
    let me = JSON.parse(localStorage.getItem('me'));
    if (me){
      return true;
    }

    return false;
  }


}
