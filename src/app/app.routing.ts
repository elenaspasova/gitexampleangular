import {Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
// import {PricingComponent} from "./pricing/pricing.component";
// import {FaqComponent} from "./faq/faq.component";
// import {LoginComponent} from "./login/login.component";
// import {ForgottenComponent} from "./forgotten/forgotten.component";
// import {DoResetComponent} from "./do-reset/do-reset.component";
// import {LogoutComponent} from "./logout/logout.component";
// import {AuthGuard} from "./auth-guard.service";
// import {SecondaryComponent} from "./secondary/secondary.component";
// import {ContactUsComponent} from "./contact-us/contact-us.component";
// import {AboutComponent} from "./about/about.component";
// import {ActorsAndModelsComponent} from "./actors-and-models/actors-and-models.component";
// import {TypecastComponent} from "./typecast/typecast.component";
// import {TypecastsComponent} from "./typecasts/typecasts.component";
// import {BlogComponent} from "./blog/blog.component";
// import {BlogPostComponent} from "./blog-post/blog-post.component";
import {AuthGuard} from './auth-guard.service';
import {AboutComponent} from './about/about.component';

export const appRoutes: Routes = [
  { path: 'home',
    component: HomeComponent,
    pathMatch: 'full'
  },
  { path: 'about',
    component: AboutComponent,
    pathMatch: 'full'
  },
  // { path: 'profile/:id',
  //   canActivate: [AuthGuard],
  //   loadChildren: './profile/profile.module#ProfileModule'
  // },
  { path: '**', redirectTo: 'home' }
];
