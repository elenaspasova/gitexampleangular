import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegisterModalComponent} from "../register-modal/register-modal.component";
import {ModalDirective} from "ng2-bootstrap";
import {ModalComunicationService} from "../../modal-comunication.service";

@NgModule({
  imports: [
    CommonModule,
    RegisterModalComponent,
    ModalDirective
  ],
  providers:[ModalComunicationService],
  declarations: []
})
export class MenuModule { }
