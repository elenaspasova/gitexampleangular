import {Component, OnInit} from "@angular/core";
import {ModalComunicationService} from "../../modal-comunication.service";
import {Router} from "@angular/router";
import {ComService} from "../../com-service.service";

@Component({
  selector: 'app-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.css']
})

export class MenuComponent implements OnInit {

  isCollapsed: boolean = false;
  langFlag = '/assets/flags/United_States_of_America.png';
  activeLang = localStorage.getItem('activeLang');
  public me = JSON.parse(localStorage.getItem('me'));
  public notifications = JSON.parse(localStorage.getItem('notifications'));
  token = localStorage.getItem('token');
  role = null;
  subcription;
  langFlags = {
    da: '/assets/flags/Denmark.png',
    no: '/assets/flags/Norway.png',
    sv: '/assets/flags/Sweden.png',
    en: '/assets/flags/United_States_of_America.png'
  };
  subscription;

  constructor(
    private _modalComunication: ModalComunicationService,
    private router: Router,
    private _comService: ComService,
  ) {
    console.log(this.notifications);
    this.activeLang = localStorage.getItem('activeLang');
    this.langFlag = this.langFlags[this.activeLang];
    let localMe = localStorage.getItem('me');
    if(localMe) {
      this.me = JSON.parse(localMe);
      this.role = this.me.role;
    }
    this.subcription = router.events.subscribe((val) => this.isCollapsed = false);
    this.subscription = _modalComunication.modalRegisterShouldOpen$.subscribe(
      mission => {
        if (mission === 'lang') {

          this.activeLang = localStorage.getItem('activeLang');
          this.langFlag = this.langFlags[this.activeLang];
        }

        if (mission === 'notifications'){
          console.log('refreshing notifications');
        }

        if (mission === 'login') {
          let localMe = localStorage.getItem('me');
          if(localMe) {
            this.me = JSON.parse(localMe);
            this.role = this.me.role;
          }
        }
      });

  }

  navigateByLink(link, id, index){
    this.notifications.splice(index, 1);
    localStorage.setItem('notifications',JSON.stringify(this.notifications));
    this._comService.post('/notification_visited',{id: id}).subscribe();
    this.router.navigateByUrl(link);
  }

  // sendCountryCode(){
  //
  //     this._comService.get('/client/v4/zones/079df31e351f1cc46a03b2696124570f/settings/ip_geolocation','https://api.cloudflare.com')
  //       .subscribe(res => {
  //         if (res.success) {
  //           console.log('res 1', res);
  //           return res;
  //         } else {
  //           console.log('res 2 on fail', res);
  //           return res;
  //         }
  //       });
  //
  // }
    // this._comService.post('/country_code', {
    // code:'12345el'
    // }, true)
    //   .subscribe(res => {
    //     if (res.success) {
    //       console.log('res on success', res.message);
    //       return res
    //     } else {
    //       console.log('res on fail', res.message);
    //       return res;
    //     }
    //   });

  announce() {
    this._modalComunication.announceMission('modal');

  }

  ngOnInit() {

  }

  // ngAfterViewInit() {
  //   this.sendCountryCode();
  // }

  // changeLanguage(ln) {
  //
  //   window.location.href = window.location.pathname + '?lang=' + ln;
  // }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.subcription.unsubscribe();
  }


}
