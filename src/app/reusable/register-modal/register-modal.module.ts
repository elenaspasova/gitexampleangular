import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ModalComunicationService} from "../../modal-comunication.service";
//import {MultiselectDropdownModule} from 'angular-2-dropdown-multiselect';
import {SharedModule} from "../../shared/shared.module";
 import {MultiselectDropdownModule} from "angular-2-dropdown-multiselect/src/multiselect-dropdown";

@NgModule({
  imports: [
    SharedModule,
    MultiselectDropdownModule,
    CommonModule
  ],
  providers: [ModalComunicationService],
  declarations: []
})
export class RegisterModalModule { }
