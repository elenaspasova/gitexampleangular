import {Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import {ModalDirective} from "ng2-bootstrap";
import {ModalComunicationService} from "../../modal-comunication.service";
import {appConfig} from "../../config/app.config";
import {ComService} from "../../com-service.service";

import {
  IMultiSelectOption,
  IMultiSelectSettings,
  IMultiSelectTexts
} from "angular-2-dropdown-multiselect/src/multiselect-dropdown";

import { isValidNumber } from 'libphonenumber-js'

import {Router} from "@angular/router";
// import {TranslateService} from "../../translate.service";
declare var swal: any;
declare const fbq: any;
@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styleUrls: ['./register-modal.component.css']
})
export class RegisterModalComponent implements OnInit {

  @ViewChild('staticModal')
  modalStatic: ModalDirective;
  subscription;
  emailPattern = this._conf.testEmail;
  role = null;
  activeLang = localStorage.getItem('activeLang');
  signUpErrors = null;
  email = null;
  password = null;
  conf_password = null;
  fName = null;
  lName = null;
  transferringData = false;
  terms = false;


  selectedItems=[];
  director = {
    agType: null,
    phone: null,
    country: null,
    companyName: null,
    companyURI: null,
    imdb: null,
    vat: null
  };

  choose = 'Choose';
  private eyeColorTexts: IMultiSelectTexts = {
    defaultTitle: this.choose

  };
  // choose: "Choose";
  directorOptions:IMultiSelectOption[] = null;
  country: IMultiSelectOption[] = null;

  private mySettingsWithSearchMultiple: IMultiSelectSettings = {
    pullRight: false,
    enableSearch: true,
    checkedStyle: 'checkboxes',
    buttonClasses: 'btn btn-default',
    selectionLimit: 0,
    closeOnSelect: false,
    showCheckAll: false,
    showUncheckAll: false,
    dynamicTitleMaxItems: 1,
    maxHeight: '300px',

  };

  constructor(
    private _modalComunication: ModalComunicationService,
    private _conf: appConfig,
    private _comService: ComService,
    private _router: Router,
   // private _TranslateService: TranslateService,
  ) {


    this.eyeColorTexts = {
      defaultTitle: this.choose,
    };
    this.subscription = _modalComunication.modalRegisterShouldOpen$.subscribe(
      mission => {
        if (mission === 'modal') {
          this._comService.get('/assets/json/director_' + this.activeLang + '.json', this._conf.baseURI).subscribe(
            res => {
              //res = res.sort(this.compare);

              this.directorOptions = res;
            }
          );
//           this._comService.get('/countries').subscribe(
//             res => {
//               if (res.success) {
//                 this.country = [];
//                 res.message.forEach((element,index) => {
//                   this.country.push({id:element._id,name: element.country});
//                  // console.log('country', this.country);
//
//
//                 });
//
// //console.log('country', this.country);
//                 if (this.activeLang === 'da') {
//                   this.director.country= this.country.slice(52,53).map(a => a);
//                   // console.log('mapped country', this.director.country);
//                   this.director.country[0] =  this.director.country[0].id;
//
//
//                 } else if (this.activeLang === 'no') {
//                   // this.director.country='58d54e4d8362073283fee2a6';
//                   this.director.country= this.country.slice(150,151).map(a => a);
//                   //console.log('mapped country', this.director.country);
//                   this.director.country[0] =  this.director.country[0].id;
//
//                 } else if (this.activeLang === 'sv') {
//                   //this.director.country='58d5514b8362073283fee2d0';
//                   this.director.country= this.country.slice(192,193).map(a => a);
//                   //console.log('mapped country', this.director.country);
//                   this.director.country[0] =  this.director.country[0].id;
//
//                 }
//
//
//
//               }
//
//
//               //this.country = res;
//             }
//           );
          let checkIsLoggedIn = JSON.parse(localStorage.getItem('me'));
          if (checkIsLoggedIn && checkIsLoggedIn._id && checkIsLoggedIn.role === 0) {
            this._router.navigate(['/payments']);
            return;
          } else if (checkIsLoggedIn && checkIsLoggedIn._id){
            setTimeout(() => swal({
              title: 'Hey Dude!',
              text: 'You are already registered user. Log out before trying to sign up again, right?',
              type: 'warning'
            }), 500);

          } else {
            this.modalStatic.show();
          }

        }
      });
    this.subscription = _modalComunication.modalRegisterShouldOpen$.subscribe(
      mission => {
        if (mission === 'lang') {

          this.activeLang = localStorage.getItem('activeLang');

        }
      });
  }

  translateInlineMessages(textToTranslate){
    if (this.activeLang === 'en') return textToTranslate;

    let lang = JSON.parse(localStorage.getItem('lang_' + this.activeLang));
    if (!lang[textToTranslate]) {
      lang[textToTranslate] = 'Missing Translation';
    }
    return lang[textToTranslate];
  }




  onChangeRouteService(event, first, country?) {
    console.log('director2', this.director.country);

    if (this.director[first].length > 1) {
      let newOne = this.director[first][this.director[first].length - 1];
      this.director[first] = [newOne];

    }
  }

  compare(a, b) {
    if (a.name < b.name)
      return -1;
    if (a.name > b.name)
      return 1;
    return 0;
  }

  ngOnInit() {
  }


  ngAfterViewInit(){

  }

  doRegister() {
    fbq('track', 'CompleteRegistration');
    this.signUpErrors = null;
    if (this.role == null) {
      this.signUpErrors =this.translateInlineMessages("Are you an actor or a filmmaker?");
      return;
    }
    if (this.email == null || this.password == null || this.conf_password == null || this.fName == null || this.lName == null) {
      this.signUpErrors = 'All fields are obligatory.';
      return;
    }

    if (this.role === '1' && (!!!this.director.phone)) {
      this.signUpErrors = 'Please fill your phone number';
      return;
    }

    if (this.role === '1' && ( (!!!this.director.agType) || (!!!this.director.country)   || (!!!this.director.phone) ) ) {
      this.signUpErrors = 'All Company information is required.';
      return;
    }

    if (this.director.imdb &&
      this.director.imdb.replace('http://www.imdb.com','') === this.director.imdb &&
      this.director.imdb.replace('https://www.imdb.com','') === this.director.imdb) {
      this.signUpErrors = 'Invalid IMDB Url';
      return;
    }

    if (this.director.companyURI &&
      this.director.companyURI.replace('http://','') === this.director.companyURI &&
      this.director.companyURI.replace('https://','') === this.director.companyURI) {
      this.signUpErrors = 'Invalid Company Url';
      return;
    }

    if (this.password !== this.conf_password) {
      this.signUpErrors = 'Password and Repeat password should match.';
      return;
    }

    if (this.emailPattern.test(this.email) !== true) {
      this.signUpErrors = 'Provided email is not valid.';
      return;
    }

    if (this.terms === false) {
      this.signUpErrors =this.translateInlineMessages("Please accept Terms and Conditions.");
      return;
    }

    if (this.password.length < 6) {
      this.signUpErrors = this.translateInlineMessages("Your password should be at least 6 characters!")
      return;
    }

    if (this.password.replace(/\D/g, '').length === 0) {
      this.signUpErrors = this.translateInlineMessages("Your password should contain at least one number");
      return;
    }

    let specialChars = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/gi;
    if (this.password.match(specialChars) === null) {
      this.signUpErrors = this.translateInlineMessages('Passwords must contain at least one special character like -') + '! @ # $ % ^ & * ( ) _ + - = [ ] { } ; \' : " | , . < > / ?';

      return;
    }




    this.transferringData = true;

    this._comService.post('/registration', {
      email: this.email,
      password: this.password,
      role: this.role,
      fName: this.fName,
      lName: this.lName,
      displayName: this.fName + ' ' + this.lName,
      director: (this.director.agType) ? this.director:null
    }, true)
      .subscribe(res => {
        this.transferringData = false;
        if (res.success) {
          this.modalStatic.hide();
          setTimeout(() => swal({
            title: 'Success',
            text:this.translateInlineMessages(res.message),
            type: 'success'
          }), 500);
          this.resetData();
          this.closeModal();
        } else {
          this.signUpErrors=this.translateInlineMessages(res.message);
          this.closeModal();
        }
      });

  }

  resetData() {
    this.role = null;
    this.signUpErrors = null;
    this.email = null;
    this.password = null;
    this.conf_password = null;
    this.fName = null;
    this.lName = null;
    this.terms = false;
  }

  readTOS() {
    window.open('assets/terms_and_conditions.html', '', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=500');
  }

  readPrivacy() {
    window.open('assets/privacy.html', '', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=500');
  }
 readData(){
   window.open('assets/data_protection.html', '', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=500');
 }


  closeModal() {
    this.resetData();
    this.modalStatic.hide();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
