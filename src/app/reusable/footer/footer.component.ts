import {Component, OnInit, ViewChild} from "@angular/core";
import {appConfig} from "../../config/app.config";
import {ComService} from "../../com-service.service";
import {ModalComunicationService} from "../../modal-comunication.service";
import {ModalDirective} from "ng2-bootstrap";

declare var swal: any;

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})

export class FooterComponent implements OnInit {

  @ViewChild('staticModal')
  modalStatic: ModalDirective;

  me = JSON.parse(localStorage.getItem('me'));
  name: string = null;
  email: any = null;
  subject: number = null;
  message: any = null;
  errorMessage: string = null;
  successMessage: string = null;
  transferringData = null;
  sendingShare = false;
  share = {
    name: null,
    email: null,
    subject: null,
    message: "Hey, look what I've found. Wonderful place!"
  };

  constructor(public _config: appConfig, private _comService: ComService) {

  }

  resetShare() {
    this.share = {
      name: null,
      email: null,
      subject: null,
      message: "Hey, look what I've found. Wonderful place!"
    };
  }

  nl2br(str) {
    if (!str) return null;

    return str.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }

  shareWithFriend() {
    if (
      this.share.name == null ||
      this.share.name == '' ||
      this.share.email == null ||
      this.share.email == '' ||
      this.share.message == null ||
      this.share.message == '',
      this.share.subject == null ||
      this.share.subject == ''
    ) {
      return swal({
        title: "Oops!",
        type: "error",
        text: "All fields are obligatory."
      })
    }
    this.sendingShare = true;
    let workShare = this.share;
    workShare.message = this.nl2br(workShare.message);

    this._comService.post('/share_with_a_friend',workShare)
      .subscribe(
        res => {
          this.sendingShare = false;
          if (res.success) {
            this.modalStatic.hide();
            this.resetShare();
            swal({
              title: "Thank You!",
              type: "success",
              text: "Thank you for sharing your awesome experience."
            })
          } else {
            swal({
              title: "Oops!",
              type: "error",
              text: res.message
            })
          }
        }
      );
  }

  subscribeEmail = null;
  subscribing = false;

  doSubscribe() {
    let email = this.subscribeEmail;
    if (!email) {
      return swal({
        title: "Oops!",
        type: "error",
        text: 'Please fill your email address.'
      });
    }
    let pattern = new RegExp(this._config.testEmail);
    if (!pattern.test(email)) {
      return swal({
        title: "Oops!",
        type: "error",
        text: 'Please check again this email - ' + email
      });
    }
    this.subscribing = true;
    this._comService.post('/mail_chimp_subscribe',{
      email: email,
      name: 'Subscriber from web'
    }).subscribe(
      (res) => {
        this.subscribing = false;
        if (res.success) {
          this.subscribeEmail = '';
          return swal({
            title: "Success!",
            type: "success",
            text: res.message
          });
        } else {
          return swal({
            title: "Oops!",
            type: "error",
            text: res.message
          });
        }
      }
    );
  }

  sendMessage() {
    this.successMessage = null;
    this.errorMessage = null;

    if (this.name == null) {
      this.errorMessage = 'Name is Obligatory';
      return;
    }

    if (this.subject == null) {
      this.errorMessage = 'Subject is Obligatory';
      return;
    }

    if (this.email == null) {
      this.errorMessage = 'Email is Obligatory';
      return;
    }

    if (this.message == null) {
      this.errorMessage = 'Message is Obligatory';
      return;
    }

    if (!this._config.testEmail.test(this.email)) {
      this.errorMessage = 'Please check the Email address!';
      return;
    }

    this.transferringData = true;
    this._comService.post('/contact_us', {
      email: this.email,
      name: this.name,
      subject: this.subject,
      message: this.message
    }, true)
      .subscribe(res => {
        this.transferringData = false;
        if (res.success) {
          this.email = null;
          this.subject = null;
          this.message = null;
          this.name = null;
          this.successMessage = res.message;

        } else {
          this.errorMessage = res.message;
        }
      });


  }



  ngOnInit() {

  }

}
