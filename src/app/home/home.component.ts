import {Component, OnInit, ViewChild} from "@angular/core";
import {ModalComunicationService} from "../modal-comunication.service";
import {Cookie} from "ng2-cookies/ng2-cookies";
import {ModalDirective} from "ng2-bootstrap";
import set = Reflect.set;
import {ComService} from "../com-service.service";
declare var jQuery: any;


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild('staticModal')
  modalStatic: ModalDirective;

  iAgree = Cookie.get('i_agree');
  activeLang = localStorage.getItem('activeLang');

  constructor(private _modalComunication: ModalComunicationService, private _comService: ComService) {
    let homeImage = new Image();
    homeImage.src = 'https://images.castifyme.com/web/images/bg-image.jpg';
    let flagEn = new Image();
    let flagDa = new Image();
    let flagNo = new Image();
    let flagSw = new Image();
    flagEn.src = 'https://www.castifyme.com/assets/flags/United_States_of_America.png';
    flagDa.src = 'https://www.castifyme.com/assets/flags/Denmark.png';
    flagNo.src = 'https://www.castifyme.com/assets/flags/Norway.png';
    flagSw.src = 'https://www.castifyme.com/assets/flags/Sweden.png';

  }
  //one time function to add value complted to profiles
  // completedUser(){
  //   console.log('completed users');
  //   this._comService.post('/is_user_complted',{}).subscribe(
  //     res => {
  //       if (res.success) {
  //         console.log('successfully completed users');
  //       }
  //
  //
  //       //this.country = res;
  //     }
  //   );
  // }

  activeVideo = null;
  playVideo() {
    this.modalStatic.show();
    let activeVideo = 'https://player.vimeo.com/video/272769431?autoplay=1';
    console.log('this.activeLang',this.activeLang);
    // https://vimeo.com/user15205645/review/272769431/0342e53ae3
    if (this.activeLang === 'da') {
      activeVideo = 'https://player.vimeo.com/video/270095625?autoplay=1';
    } else if (this.activeLang === 'no') {
      activeVideo = 'https://player.vimeo.com/video/270096598?autoplay=1';
    } else if (this.activeLang === 'sv') {
      activeVideo = 'https://player.vimeo.com/video/270096949?autoplay=1';
    }

    jQuery('#videoPlayerWrapper').append('<iframe allowfullscreen="true" style="width: 100%;height: 483px" *ngIf="!!activeVideo" id="mainIframe" src="' + activeVideo + '"' +
      'width="640"' +
      'height="360"' +
      'frameborder="0"' +
      'webkitallowfullscreen' +
      'mozallowfullscreen' +
      'allowfullscreen></iframe>');
  }

  announce() {
    this._modalComunication.announceMission('modal');

  }

  closeVideoPlayer() {
    jQuery('#videoPlayerWrapper').html('');
    this.modalStatic.hide();
  }

  setIAgree() {
    Cookie.set('i_agree', 'true',365);
    this.iAgree = 'true';
  }

  readPrivacy() {
    window.open('assets/privacy.html', '', 'directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=300,height=500');
  }

  ngOnInit() {
    //this.completedUser();
      }

}
